## Установка проекта


1. Склонировать репозиторий
```
git clone https://gitlab.com/Nurs.K/notepad_web-app.git
```
2. Перейти в директорию проекта
3. Создать virtual environment
``` 
python -m venv env
 ```

4. Активаций virtual environment
``` 
source env/bin/activate
 ```

5. Установка нужных пакетов
``` 
pip install -r requirements.txt
 ```

6. Запуск проекта
``` 
python manage.py makemigrations

python manage.py migrate

python manage.py runserver
 ```
7. Создания админа сайта
``` 
python manage.py createsuperuser
 ```

## API endpoints
The full list of API endpoints is available at the following addresses:
```
http://127.0.0.1:8000/swagger/
```

CRUD 
```
http://127.0.0.1:8000/api/v1/notepads/
```

