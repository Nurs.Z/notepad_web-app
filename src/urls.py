from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="Notepad - Web-app",
        default_version='v1',
        description="API for Notepad - Web-app",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="hello@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

api_v1_patterns = [
    path('notepads/', include('apps.notepads.urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/v1/", include(api_v1_patterns)),

    # auth
    path("auth/", include("rest_framework.urls")),

    # docs
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
