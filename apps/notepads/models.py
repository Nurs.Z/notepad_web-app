from django.db import models


class Tag(models.Model):
    title = models.CharField(
        max_length=255,
        verbose_name='tag name',
        db_index=True,
    )

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'

    def __str__(self):
        return f"Tag -- {self.title}"


class Notepad(models.Model):
    title = models.CharField(
        max_length=255,
        verbose_name='title',
        db_index=True,
    )
    description = models.TextField(
        verbose_name='description',
        blank=True, null=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    tags = models.ManyToManyField(
        Tag, related_name='tags',
        blank=True,
    )

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Notepad'
        verbose_name_plural = 'Notepads'

    def __str__(self):
        return f"{self.title} -- {self.created_at}"
