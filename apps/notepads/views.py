from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters

from apps.notepads import models
from apps.notepads import serializers
from apps.notepads.filters import NotepadFilter


class TagViewSet(viewsets.ModelViewSet):
    queryset = models.Tag.objects.all()
    serializer_class = serializers.TagSerializer


class NotepadViewSet(viewsets.ModelViewSet):
    queryset = models.Notepad.objects.all()
    serializer_class = serializers.NotepadSerializer
    filter_backends = (
        DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter
    )
    filterset_class = NotepadFilter
    search_fields = ('title',)
    ordering_fields = ('title', 'created_at',)

    def get_serializer_class(self):
        if self.action in ['retrieve']:
            return serializers.NotepadDetailSerializer
        return self.serializer_class
