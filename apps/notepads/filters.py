import django_filters

from apps.notepads.models import Notepad


class NotepadFilter(django_filters.FilterSet):
    tags = django_filters.CharFilter(
        field_name='tags__title',
        lookup_expr='icontains',
    )

    class Meta:
        model = Notepad
        fields = ('tags',)
