from django.contrib import admin

from apps.notepads import models


@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_filter = ('title',)


@admin.register(models.Notepad)
class NotepadAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'created_at',)
    list_filter = ('title', 'created_at',)
    search_fields = ('title',)
