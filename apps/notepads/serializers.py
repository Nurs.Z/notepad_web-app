from rest_framework import serializers

from apps.notepads import models


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tag
        fields = (
            'id',
            'title',
        )


class NotepadSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Notepad
        fields = (
            'id',
            'title',
            'description',
            'created_at',
            'tags',
        )


class NotepadDetailSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = models.Notepad
        fields = (
            'title',
            'description',
            'created_at',
            'tags',
        )
