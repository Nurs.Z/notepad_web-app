from rest_framework.routers import DefaultRouter

from apps.notepads import views

router = DefaultRouter()
router.register('notepads', views.NotepadViewSet, basename='notepads')
router.register('tags', views.TagViewSet, basename='tags')

urlpatterns = router.urls
